import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Header from './Header';
import Image_01 from  '../assets/images/keystone_banner.jpg';
import Image_02 from '../assets/images/latorga_banner.jpg';
// eslint-disable-next-line no-unsaved-vars
import Footer from './Footer';

class AboutUs extends Component {
    render() {
      return (
  <div className="">
      <Header />
    <section>
  	<div className="container-fluid" id="aboutus">
  		<div className="container">
  			<div className="row pb-0">
  				<div className="col text-center">
  					<h1 className="green">FAQs</h1>
  					<p className="color7">Frequently asked questions about the Keystone blockchain and the La Tortuga community.</p>
  				</div>
  			</div>

        <ul className="nav nav-tabs" role="tablist">
          <li className="nav-item">
            <a className="nav-link" href="#Keystone-Blockchain" role="tab" data-toggle="tab"><img className="rounded img-responsive" src={../assets/images/keystone_banner.jpg} alt=""/></a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#La-Tortuga" role="tab" data-toggle="tab"><img className="rounded img-responsive" src={../assets/images/latorga_banner.jpg} alt=""/></a>
          </li>
        </ul>
        
        <div className="tab-content">
          <div role="tabpanel" className="tab-pane fade active show" id="Keystone-Blockchain">
                
              <div id="Keystone_BC_accordion">
                <div className="card">
                  <div className="card-header" id="1">
                    <h3 className="mb-0 display-6 color7" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                     What is Keystone?
                    </h3>
                  </div>
                  <div id="collapse1" className="collapse" aria-labelledby="1" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                      Keystone is a currency that will be the primary form of payment in La Tortuga, 	Mexico. It will be used for all transactions once the development is fully operational. You will be able to rent hotel rooms, buy dinner, gamble, pay for excursions, and many more possibilities!
                    </div>
                  </div>
                </div>
                
                <div className="card">
                  <div className="card-header" id="2">
                    <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                    What is the max coin supply?
                    </h3>
                  </div>
                  <div id="collapse2" className="collapse" aria-labelledby="2" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                      Keystone’s max supply is 150,000,000.
                    </div>
                  </div>
                </div>
                
                <div className="card">
                  <div className="card-header" id="8">
                    <h3 className="mb-0 display-6  color7 collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                    Can I mine Keystone?
                    </h3>
                  </div>
                  <div id="collapse8" className="collapse" aria-labelledby="8" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                      <p>Yes, mining operations will be open to the public once the blockchain code is released. Mining software can be developed, or the mining program developed by the Keystone blockchain developers can be used.</p>
                    </div>
                  </div>
                </div>
    					  
                <div className="card">
                  <div className="card-header" id="9">
                    <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
                      Can I earn money with mining?
                    </h3>
                  </div>
                  <div id="collapse9" className="collapse" aria-labelledby="9" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                      <p>There are many way’s to earn money using blockchain technology, Keystone just 	brings a few real world uses to the table.</p>
                    </div>
                  </div>
                </div>
                
                <div className="card">
                  <div className="card-header" id="10">
                    <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
                      Can I setup a node?
                    </h3>
                  </div>
                  <div id="collapse10" className="collapse" aria-labelledby="10" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                      Yes. (<a href="https://www.dropbox.com/s/y69alf9wenr3431/Setup Node.txt?dl=0">Follow this wiki</a>)
                    </div>
                  </div>
                </div>
                
                <div className="card">
                  <div className="card-header" id="11">
                    <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                  	 Where can I keep my Keystone?
                  	</h3>
                  </div>
                  <div id="collapse11" className="collapse" aria-labelledby="11" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                    	You don’t need to “keep your keystone” anywhere. They are always on the chain. 	Access can be had through any open node with private key signatures.
      						  </div>
        					</div>
      				  </div>

      				  <div className="card">
        					<div className="card-header" id="12">
        					  <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
          						How do I access my Keystone?
          				  </h3>
                  </div>
                  <div id="collapse12" className="collapse" aria-labelledby="12" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                    	Access to Keystone is granted through using any web interface application. 	Specification for developing one can be found here. All nodes come with a wallet 	interface, it’s up to the node if the run it.
       						  </div>
       						</div>
     					  </div>

     					  <div className="card">
       						<div className="card-header" id="13">
       							<h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">
         							 How do I secure my Keystone?
       						  </h3>
       						</div>
        					<div id="collapse13" className="collapse" aria-labelledby="13" data-parent="#Keystone_BC_accordion">
              		  <div className="card-body">
                 			Your Keystone are secured on the chain, by your private key. Keep you private key 	safe and your Keystone remains safe.
              		  </div>
              		</div>
            	  </div>
    
     					  <div className="card">
          				<div className="card-header" id="14">
       							<h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
                      I lost my private key, is there a way to retrieve it?
                    </h3>
                  </div>
                  <div id="collapse14" className="collapse" aria-labelledby="14" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                      There is no way to return your private key.
                    </div>
                  </div>
                </div>
                
                <div className="card">
                  <div className="card-header" id="15">
                  	<h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse15" aria-expanded="false" aria-controls="collapse15">
                      How do I send Keystone?
                    </h3>
                  </div>
                  <div id="collapse15" className="collapse" aria-labelledby="15" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                      Send a transaction via any web interface by verifying you have the private key to do so.
                    </div>
                  </div>
                </div>

                <div className="card">
                  <div className="card-header" id="16">
                    <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse16" aria-expanded="false" aria-controls="collapse16">
                      What are the transaction fees?
                    </h3>
                  </div>
                  <div id="collapse16" className="collapse" aria-labelledby="16" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                      Transaction fees vary from node to node, and from pow to pos. See here for more information.
                    </div>
                  </div>
                </div>

     					  <div className="card">
       						<div className="card-header" id="17">
       							<h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse17" aria-expanded="false" aria-controls="collapse17">
         							 What is the lowest denomination of Keystone?
       						  </h3>
       						</div>
       						<div id="collapse17" className="collapse" aria-labelledby="17" data-parent="#Keystone_BC_accordion">
       						  <div className="card-body">
         							Six decimal places, 0.000001.
       						  </div>
       						</div>
      				  </div>

          		  <div className="card">
            			<div className="card-header" id="18">
       							<h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse18" aria-expanded="false" aria-controls="collapse18">
        						 Is there a mobile wallet?
            			  </h3>
            			</div>
            			<div id="collapse18" className="collapse" aria-labelledby="18" data-parent="#Keystone_BC_accordion">
            			  <div className="card-body">
              				All wallets are mobile, as they are the private key, as long as you have it you can use 	any wallet interface to perform transactions.
            			  </div>
            			</div>
          		  </div>

     					  <div className="card">
       						<div className="card-header" id="19">
       							<h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse19" aria-expanded="false" aria-controls="collapse19">
       							 What is Proof of Stake?
       						  </h3>
       						</div>
       						<div id="collapse19" className="collapse" aria-labelledby="19" data-parent="#Keystone_BC_accordion">
       						  <div className="card-body">
          						Proof of stake (PoS) is a type of algorithm by which a cryptocurrency blockchain 	network aims to achieve distributed consensus. In PoS-based cryptocurrencies, the 	creator of the next block is chosen via various combinations of random selection and 	wealth or age (i.e., the stake).
                	  </div>
                	</div>
                </div>
    
     					  <div className="card">
       						<div className="card-header" id="20">
       							<h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse20" aria-expanded="false" aria-controls="collapse20">
         							 When does Proof of Stake start?
       						  </h3>
       						</div>
       						<div id="collapse20" className="collapse" aria-labelledby="20" data-parent="#Keystone_BC_accordion">
       						  <div className="card-body">
          						Proof of Stake will start when circulation supply hits 35m, or you see a vampire.
                	  </div>
       						</div>
                </div>

                <div className="card">
                  <div className="card-header" id="21">
                  	<h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse21" aria-expanded="false" aria-controls="collapse21">
                     What are the Proof of Stake rewards?
 									  </h3>
                  </div>
                  <div id="collapse21" className="collapse" aria-labelledby="21" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                      Our POS system has not been worked out yet, therefor we cannot reveal the rewards 	yet.
                    </div>
                  </div>
                </div>

     					  <div className="card">
                  <div className="card-header" id="22">
                  	<h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse22" aria-expanded="false" aria-controls="collapse22">
                      Who owns Keystone?
                    </h3>
                  </div>
                  <div id="collapse22" className="collapse" aria-labelledby="22" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                      No one owns the chain code, the developer made that public, but Keystone Currency Inc. Owns the exchange.
                    </div>
                  </div>
                </div>
                
                <div className="card">
                  <div className="card-header" id="23">
                    <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse23" aria-expanded="false" aria-controls="collapse23">
                      Is the Keystone source public?
                    </h3>
                  </div>
                  <div id="collapse23" className="collapse" aria-labelledby="23" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                      We are keeping in private until we launch, then we will publish it and launch at the same time.
                    </div>
                  </div>
                </div>
                
                <div className="card">
                  <div className="card-header" id="24">
                    <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse24" aria-expanded="false" aria-controls="collapse24">
                      Where can I find the Roadmap?
                    </h3>
                  </div>
                  <div id="collapse24" className="collapse" aria-labelledby="24" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                      <a href="/Roadmap">Check Roadmap</a>
                    </div>
                  </div>
                </div>

                <div className="card">
                  <div className="card-header" id="25">
                  	<h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse25" aria-expanded="false" aria-controls="collapse25">
                    	Where can I find the Whitepaper?
                    </h3>
                  </div>
                  <div id="collapse25" className="collapse" aria-labelledby="25" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
                    	<a href="/Litepaper">Check Whitepaper</a>
                    </div>
                  </div>
                </div>

                <div className="card">
                  <div className="card-header" id="26">
                		<h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse26" aria-expanded="false" aria-controls="collapse26">
                  		What is the Keystone Exchange?
                	  </h3>
                	</div>
                	<div id="collapse26" className="collapse" aria-labelledby="26" data-parent="#Keystone_BC_accordion">
                	  <div className="card-body">
                  		The Keystone Exchange is your source for keystone while others create their 	exchanges. We got the good’s while you get the goods!
                	  </div>
                	</div>
                </div>

                <div className="card">
                	<div className="card-header" id="27">
                		<h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse27" aria-expanded="false" aria-controls="collapse27">
                  		Which trading pairs does the Keystone Exchange support?
                	  </h3>
                	</div>
                	<div id="collapse27" className="collapse" aria-labelledby="27" data-parent="#Keystone_BC_accordion">
                	  <div className="card-body">
                  		At the beginning the Keystone Exchange supports KEYS/FIAT. Later on there will be 	KEYS/BTC pairs and other cryptocurrencies.
                	  </div>
                	</div>
                </div>

                <div className="card">
                	<div className="card-header" id="28">
                		<h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse28" aria-expanded="false" aria-controls="collapse28">
                  		What are the official sources of news from Keystone?
                	  </h3>
                	</div>
                	<div id="collapse28" className="collapse" aria-labelledby="28" data-parent="#Keystone_BC_accordion">
                	  <div className="card-body">
                  		<a href="https://twitter.com/Keystone_KEYS">Twitter</a> | <a href="https://www.facebook.com/KeystoneCurrency/">Facebook</a> | <a href="https://www.instagram.com/keystonecurrency/">Instagram</a> | <a href="https://discord.gg/vjbXeHf">Discord</a> | <a href="https://t.me/joinchat/AAAAAEyeJE3cAzcZjG8OHg">Telegram</a> | <a href="https://www.reddit.com/user/KeystoneCurrency/">Reddit</a>
                	  </div>
       						</div>
     					  </div>

 
                <div className="card">
                  <div className="card-header" id="29">
                    <h3 className="mb-0 display-6 color7" data-toggle="collapse" data-target="#collapse29" aria-expanded="true" aria-controls="collapse29">
                     Will Keystone Currency be used only in La Tortuga? Or will Keystone Currency be developed and used in other regions later?
                    </h3>
                  </div>
                  <div id="collapse29" className="collapse" aria-labelledby="29" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
Keystone will initially be developed for the La Tortuga development in Mexico. It is, however, a scalable project and there are future developments already earmarked. First and foremost though, we are focusing on La Tortuga. 
                    </div>
                  </div>
                </div>
                <div className="card">
                  <div className="card-header" id="30">
                    <h3 className="mb-0 display-6 color7" data-toggle="collapse" data-target="#collapse30" aria-expanded="true" aria-controls="collapse30">
                     How many people are working on the Keystone Currency project?
                    </h3>
                  </div>
                  <div id="collapse30" className="collapse" aria-labelledby="30" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
We have a team of 13 actively working on this project ranging from marketers and writers to experienced web and programming devs. We cover all bases but we're always looking for top talent to contribute. 
                    </div>
                  </div>
                </div>

                <div className="card">
                  <div className="card-header" id="31">
                    <h3 className="mb-0 display-6 color7" data-toggle="collapse" data-target="#collapse31" aria-expanded="true" aria-controls="collapse31">
                     What sort of rig will i need to mine for the Bcrypt algo?
                    </h3>
                  </div>
                  <div id="collapse31" className="collapse" aria-labelledby="31" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
We are using the bcrypt algorithm to enable fair mining for all. Bcrypt should make it good for all to mine, whether you have a powerful rig or not.    
                </div>
                  </div>
                </div>


                <div className="card">
                  <div className="card-header" id="32">
                    <h3 className="mb-0 display-6 color7" data-toggle="collapse" data-target="#collapse32" aria-expanded="true" aria-controls="collapse32">
                     When is Keystone Currency getting listed on an exchange?
                    </h3>
                  </div>
                  <div id="collapse32" className="collapse" aria-labelledby="32" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
After the coin is launched, we will start the process of getting listed on exchanges, whilst building our own exchange.  <br/>

To get listed, we will need to prove our coins worth by way of a good product and a great community so this is something we are working on continuously.
                </div>
                  </div>
                </div>

                <div className="card">
                  <div className="card-header" id="33">
                    <h3 className="mb-0 display-6 color7" data-toggle="collapse" data-target="#collapse33" aria-expanded="true" aria-controls="collapse33">
                     How will moving from POW to POS be handled?
                    </h3>
                  </div>
                  <div id="collapse33" className="collapse" aria-labelledby="33" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
At 35m coins in circulation, the code will switch over seamlessly to our PoS system.  The code for the switch has been written, but as for the actual PoS rewards and finer details, we are yet to confirm those.  We are building an economy around this coin, so we have to make sure we get it right.  We have until the coin is mined to 35m before we implement the PoS but we assure you this will be done long before we reach that.
                </div>
                  </div>
                </div>

                <div className="card">
                  <div className="card-header" id="34">
                    <h3 className="mb-0 display-6 color7" data-toggle="collapse" data-target="#collapse34" aria-expanded="true" aria-controls="collapse34">
                     Does the government support La Tortuga  and Keystone?
                    </h3>
                  </div>
                  <div id="collapse34" className="collapse" aria-labelledby="34" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
If we did not have the support of local, state, and federal government. We could not make the statements that we have made. La Tortuga was an ongoing project well before the incorporation of Keystone. 
Keystone has now brought us a opportunity that meets our standards for our community. 
A true decentralized wealth system. To be blended into our 100&#37; off grid community.                </div>
                  </div>
                </div>


                <div className="card">
                  <div className="card-header" id="35">
                    <h3 className="mb-0 display-6 color7" data-toggle="collapse" data-target="#collapse35" aria-expanded="true" aria-controls="collapse35">
                     Will there be a bounty program?
                    </h3>
                  </div>
                  <div id="collapse35" className="collapse" aria-labelledby="35" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
Yes, later on there will be.
</div>
                  </div>
                </div>

               <div className="card">
                  <div className="card-header" id="36">
                    <h3 className="mb-0 display-6 color7" data-toggle="collapse" data-target="#collapse36" aria-expanded="true" aria-controls="collapse36">
                     Will there be a downloadable wallet?
                    </h3>
                  </div>
                  <div id="collapse36" className="collapse" aria-labelledby="36" data-parent="#Keystone_BC_accordion">
                    <div className="card-body">
Its a brand new blockchain written from scratch.  We are not forking or copying another coin with core wallet features.  We are starting with a web based wallet using RSA signatures.  This will allow us to be compatible on almost every device.  We will look to release downloadable wallets in the future.
But with it being javascript based, you will be able to run a node or miner on most devices.
</div>
                  </div>
                </div>





              </div>
          </div>
          <div role="tabpanel" className="tab-pane fade" id="La-Tortuga">
            <div id="accordion">
              <div className="card">
                <div className="card-header" id="1">
                  <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#La-Tortuga_collapse1" aria-expanded="true" aria-controls="La-Tortuga_La-Tortuga_collapse1">
                    What is La Tortuga?
                  </h3>
                </div>
                <div id="La-Tortuga_collapse1" className="collapse" aria-labelledby="1" data-parent="#accordion">
                  <div className="card-body">
                  La Tortuga will be the first eco-friendly, off-grid 21st century city. A full service 	community with hospitals attracting the world’s best practitioners, schools staffed by the best educators, a place where utilities such as power and water, waste management, heating and air conditioning have been designed from the beginning to achieve the minimum carbon footprint whilst maximising the standard of living for all our residents.
                  </div>
                </div>
              </div>
            
              <div className="card">
                <div className="card-header" id="3">
                  <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                    Where is La Tortuga?
                  </h3>
                </div>
                <div id="collapse3" className="collapse" aria-labelledby="3" data-parent="#accordion">
                  <div className="card-body">
                    La Tortuga is located in Puerto Peñasco municipality in the state of Sonora, Mexico.
                  </div>
                </div>
              </div>
            
              <div className="card">
                <div className="card-header" id="29">
                  <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse29" aria-expanded="false" aria-controls="collapse29">
                    Where is the La Tortuga community located?
                  </h3>
                </div>
                <div id="collapse29" className="collapse" aria-labelledby="29" data-parent="#accordion">
                  <div className="card-body">
                    It is located in Puerto Pe&ntilde;asco municipality in the state of Sonora, Mexico.
                  </div>
                </div>
              </div>
            
              <div className="card">
                <div className="card-header" id="30">
                  <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse30" aria-expanded="false" aria-controls="collapse30">
                    How much has been invested in the La Tortuga development thus far?
                  </h3>
                </div>
                <div id="collapse30" className="collapse" aria-labelledby="30" data-parent="#accordion">
                  <div className="card-body">
                    Around $20,000,000.00 USD
                  </div>
                </div>
              </div>
            
              <div className="card">
                <div className="card-header" id="31">
                  <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse31" aria-expanded="false" aria-controls="collapse31">
                    How much land will be developed?
                  </h3>
                </div>
                <div id="collapse31" className="collapse" aria-labelledby="31" data-parent="#accordion">
                  <div className="card-body">
                    3,000 hectares total. Phase 1 will consist of 500 hectares
                  </div>
                </div>
              </div>
            
              <div className="card">
                <div className="card-header" id="32">
                  <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse32" aria-expanded="false" aria-controls="collapse32">
                    What stage of development is the land in?
                  </h3>
                </div>
                <div id="collapse32" className="collapse" aria-labelledby="32" data-parent="#accordion">
                  <div className="card-body">
                    All zoning has been approved on the entire property for development. 
                  </div>
                </div>
              </div>
            
              <div className="card">
                <div className="card-header" id="33">
                  <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse33" aria-expanded="false" aria-controls="collapse33">
                    Will there be a Cruise port near La Tortuga?
                  </h3>
                </div>
                <div id="collapse33" className="collapse" aria-labelledby="33" data-parent="#accordion">
                  <div className="card-body">
                    The cruise port is being built as we speak. The Federal Government has recently budgeted and approved $80,000,000.00 to continue construction. Plus a recent coalition between the Puerto Penasco Government and the the Arizona Government in the United States was formed with sole tasc of getting the pier finished.  
                  </div>
                </div>
              </div>
            
              <div className="card">
                <div className="card-header" id="34">
                  <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse34" aria-expanded="false" aria-controls="collapse34">
                    Will the US Dollar or Mexican Peso be accepted at La Tortuga?
                  </h3>
                </div>
                <div id="collapse34" className="collapse" aria-labelledby="34" data-parent="#accordion">
                  <div className="card-body">
                    Keystone Currency will be the only accepted form of payment accepted for commerce and investment in the development.
                  </div>
                </div>
              </div>
            
              <div className="card">
                <div className="card-header" id="35">
                  <h3 className="mb-0 display-6 color7 collapsed" data-toggle="collapse" data-target="#collapse35" aria-expanded="false" aria-controls="collapse35">
                    How will vendors keep up with fluctuating price?
                  </h3>
                </div>
                <div id="collapse35" className="collapse" aria-labelledby="35" data-parent="#accordion">
                  <div className="card-body">
                    Prices will fluctuate and we understand that. Vendors will be encouraged to lock in a price every 12 hours. Our goal is to implement technology that will allow for streamlined shopping. Those who work at La Tortuga and are paid in KEYS will not have to wait for 2 weeks to receive a paycheck; this will help alleviate the burden of fluctuating price.
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  	</div>
  </section>


   <Footer />
    </div>
    );
    }
  }
  export default withRouter(AboutUs);
