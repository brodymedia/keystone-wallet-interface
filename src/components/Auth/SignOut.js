import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logout } from '../../actions';
import Header from '../Header';
class SignOut extends Component {
  componentWillMount() {
    this.props.logout();
  }

  render() {
    return (
            <div>
              <Header />
                <h1>You have signed out</h1>
            </div>
            );
  }
}

export default connect(null, { logout })(SignOut);