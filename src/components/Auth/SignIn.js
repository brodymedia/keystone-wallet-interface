import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { login } from '../../actions';
import { connect } from 'react-redux';
import Header from '../Header';
import loginButton from '../../assets/images/login_btn.png';
import Footer from '../Footer';
class SignIn extends Component {
  handleFormSubmit({ privateKey }) {
    const privateKeyHeader = '-----BEGIN PRIVATE KEY-----';
    const privateKeyFooter = '-----END PRIVATE KEY-----';
    if(privateKey === undefined || privateKey.length > 521 || privateKey.length < 460) {
      this.setState({error:"Invalid Key length"});
    } else {
      if(privateKey.length === 460) {
        let privateKeyArr = privateKey.split('');
        let _privateKey = [];
        while(privateKeyArr.length) {
          let segment = privateKeyArr.slice(0, 64);
          _privateKey.push(segment.join(''));
          privateKeyArr = privateKeyArr.slice(64);//
        }
        _privateKey.unshift(privateKeyHeader);
        _privateKey.push(privateKeyFooter);
        privateKey = _privateKey.join(' ');
      }
      this.props.login(privateKey, this.props.history);
    }
  }
  handleUploadFile = (event) => {
    const reader = new FileReader();
    reader.readAsBinaryString(event.target.files[0]);
    reader.onload = () => {
      this.props.login(reader.result.split('\\n').join('\n'), this.props.history);
    };
  }
  renderAlert() {
    if (!this.props.error) return null;
    return <h3>{this.props.error}</h3>;
  }
  loadKeyFromLocalStorage() {
    const privateKey = localStorage.getItem('privateKey');
    this.props.login(privateKey, this.props.history);
  }
  render() {
    const { handleSubmit } = this.props;
    return (
    <div>
      <section>
          <Header />
      </section>
      <section>
        <div className="container-fluid" id="aboutus">
          <div className="container">
            <div className="row ">
              <div className="col text-center">
                <h1 className="green">Login To Your <span style={{textShadow: '2px 2px 4px black'}}>Keystone</span> Wallet</h1>
                <p className="color16">Note: Don&#39;t lose your wallet! You want to store your private key in a safe place for accessing your wallet.</p>
              </div>
            </div>
            <div className="m-5">
              {this.renderAlert()}
            </div>
            <div className="row">
              <div className="col-12 col-sm-9 col-md-10 col-lg-10">
                <form  onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                    <Field className="form-control" rows="4" name="privateKey" component="textarea" type="textarea" />
                </form>
              </div>
              <div className="col-12 col-sm-3 col-md-2 col-lg-2">
                <button action="submit"><img src={loginButton} alt="Sign In"/></button>
              </div>
            </div>
            <div className="m-5"></div>
            <div className="row">
              <div className="col text-center">
                <hr/>
              </div>
            </div>
            <div className="m-2"></div>
            <div className="row justify-content-center text-center">
              <div className="col align-self-center">
                <button className="btn btn-lg green_bg w-100" onClick={this.loadKeyFromLocalStorage.bind(this)}>Browser Login</button>
              </div>
              <div className="badge badge-secondary align-self-center color8-bg">OR</div>
              <div className="col ">
                <input className="btn btn-lg green_bg w-100" onChange={this.handleUploadFile.bind(this)} name="privateKeyFile" type="file"/>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </div>);
  }
}

const mapStateToProps = state => {
  return {
    error: state.auth.error,
    authenticated: state.auth.authenticated
  };
};

SignIn = connect(mapStateToProps, { login })(SignIn);

export default reduxForm({
  form: 'signin',
  fields: ['privateKey']
})(SignIn);
