import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import menulogo from '../assets/images/menu_logo.png';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
  } from 'reactstrap';
  import logo from '../assets/images/logo.png';
  import facebook from '../assets/images/facebook.png';
  import twitter from '../assets/images/twitter.png';
  import instagram from '../assets/images/instagram.png';
  import discord from '../assets/images/discord.png';
  import forumimage from '../assets/images/forum.png';
  import telegram from '../assets/images/telegram.png';
  import reddit from '../assets/images/reddit.png';
class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
    this.toggle = this.toggle.bind(this);
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  getLinks() {
    if (this.props.authenticated) {
      return [
        <NavItem key={1}>
          <NavLink tag={Link} to="/Block" style={{textShadow: '2px 2px 2px black'}}>Explorer</NavLink>
        </NavItem>,
        <NavItem key={2}>
          <NavLink tag={Link} to="/Wallet" style={{textShadow: '2px 2px 2px black'}}>Wallet&nbsp;Overview</NavLink>
        </NavItem>,
        <NavItem key={3}>
          <NavLink tag={Link} to="/Wallet/Send-Transaction" style={{textShadow: '2px 2px 2px black'}}>Send</NavLink>
        </NavItem>,
        <NavItem key={4}>
            <NavLink tag={Link} to="/Roadmap" style={{textShadow: '2px 2px 2px black'}}>Roadmap</NavLink>
        </NavItem>,
        <NavItem key={5}>
          <NavLink tag={Link} to="/FAQ" style={{textShadow: '2px 2px 2px black'}}>FAQ</NavLink>
        </NavItem>,
   <NavItem key={6}>
          <NavLink tag={Link} to="/About-Us" style={{textShadow: '2px 2px 2px black'}}>About&nbsp;Us</NavLink>
        </NavItem>,
   <NavItem key={7}>
          <NavLink tag={Link} to="/Litepaper" style={{textShadow: '2px 2px 2px black'}}>Litepaper</NavLink>
        </NavItem>
      ];
    }
    return [
      
	<NavItem key={1}>
        <NavLink tag={Link} to="/" className="d-block d-md-none d-lg-none d-xl-none" style={{textShadow: '2px 2px 2px black'}}>Home</NavLink>
      </NavItem>,
	<NavItem key={2}>
        <NavLink tag={Link} to="/Block" style={{textShadow: '2px 2px 2px black'}}>Explorer</NavLink>
      </NavItem>,
      <NavItem key={3}>
          <NavLink tag={Link} to="/Roadmap" style={{textShadow: '2px 2px 2px black'}}>Roadmap</NavLink>
      </NavItem>,
      <NavItem key={4}>
          <NavLink tag={Link} to="/FAQ" style={{textShadow: '2px 2px 2px black'}}>FAQ</NavLink>
      </NavItem>,
   <NavItem key={5}>
          <NavLink tag={Link} to="/Litepaper" style={{textShadow: '2px 2px 2px black'}}>Litepaper</NavLink>
        </NavItem>,
      <NavItem key={6}>
          <NavLink tag={Link} to="/About-Us" style={{textShadow: '2px 2px 2px black'}}>About&nbsp;Us</NavLink>
      </NavItem>,
    ];
  }

  render() {
    return (
      <div>
        <section>
        	<div className="container-fluid bg-dark" id="top_menu">
        		<div className="container">
        			<div className="row">
              <div className="col" id="social_icons">
                <div><a href="https://www.facebook.com/KeystoneCurrency/"><img src={facebook} alt="Keystone Facebook"/></a></div>
                <div><a href="https://twitter.com/Keystone_KEYS"><img src={twitter} alt="Keystone Twitter"/></a></div>
                <div><a href="https://www.instagram.com/keystonecurrency/"><img src={instagram} alt="Keystone Instagram"/></a></div>
                <div><a href="https://discord.gg/sJvgZ8K"><img src={discord} alt="Keystone Discord"/></a></div>
                <div><a href="https://bitcointalk.org/index.php?topic=3238352.0"><img src={forumimage} alt="Keystone Bitcointalk"/></a></div>
                <div><a href="https://t.me/joinchat/AAAAAEyeJE3cAzcZjG8OHg"><img src={telegram} alt="Keystone Telegram"/></a></div>
                <div><a href="https://www.reddit.com/r/KeystoneCurrency/"><img src={reddit} alt="Keystone Reddit"/></a></div>
              </div>
              {(this.props.authenticated) ? 
        				<div className="col text-right align-self-center">
        					<a href="/Sign-In" style={{textShadow: '2px 2px 4px limegreen'}}>Sign-In</a> | <a href="/Wallet/Create" style={{textShadow: '2px 2px 4px limegreen'}}>Create A Wallet</a>
        				</div>
                : ''}
        			</div>
        		</div>
        	</div>
        </section>
        <div className="container-fluid d-none d-md-block" id="top_area">
       		<div className="container">
       			<div className="row">
       				<div className="col text-center align-self-center my-2 or my-2"><a href="/"><img src={logo} alt="Keystone Currency Blockchain"/></a></div>
       			</div>
       		</div>
       	</div>
        <div className="container-fluid mx-0 px-0" id="navigation">
        <div className="container">
          <div className="row">
            <div className="col">
              <div className="navbar--top">
                 <Navbar color="faded" light expand="md" style={{zIndex:1100}}>
                 <a className="navbar-brand" id="nav-logo" href="/" style={{display:'none'}}><img src={menulogo} alt="Keystone Currency Blockchain"/></a>
                 <NavbarToggler onClick={this.toggle} />
                  <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="mx-auto" navbar>
                      {this.getLinks()}
                    </Nav>
                  </Collapse>
                </Navbar>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>

    );
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.auth.authenticated
  };
};

export default withRouter(connect(mapStateToProps)(Header));
