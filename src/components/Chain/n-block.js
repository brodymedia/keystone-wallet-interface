import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import Header from '../Header';
import Footer from '../Footer';
import HOST_URL from '../../configs';
class NBlock extends Component {
    componentWillMount() {
      this.getBlockN();
    }
    componentDidMount() {
      //this.getBlockN();
    }
    getBlockN() {
      axios
        .get(`${HOST_URL}/Block/${this.props.match.params.blockId}`)
        .then((response) => {
          console.log(response);
          this.setState({block:response.data});
        })
        .catch((err) => {
          console.log(err);
          return 0;
        });
    }
    render() {
      return (
        <div>
          <Header />

          {this.state ?
              <div className="container-fluid" id="aboutus">
                <div className="container">
                  <div className="row ">
             				<div className="col text-center">
             					<h1 className="green">Block Explorer</h1>
             				</div>
             			</div>
                  <div className="row">
             				<div className="col-12 col-sm-12 col-md-12 col-lg-12">
             					  <div className="card" id="accordion">             			
                          <div className="card-header color8-bg color5 text-center" id="1" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                              <div className="row">
                                <div className="col-4 text-left"><strong>Block  </strong>{this.state.block.blockId}</div>
                                <div className="col-4"><strong>Forged By  </strong>{this.state.block.response.forgedBy}</div>
                                <div className="col-4 text-right"><strong>Award  </strong>{this.state.block.response.awardTotal}</div>
                              </div>
                              <div className="row">
                                <div className="col-4 text-left"><strong>Difficulty </strong>{this.state.block.response.difficulty}</div>
                                <div className="col-4"><strong>Time-Complexity </strong>{this.state.block.response.timeComplexity} </div>
                                <div className="col-4 text-right"><strong>Proof </strong>{this.state.block.response.proof}</div>
                              </div>
                              <div className="row">
                                <div className="col">
                                  <h3 className="mb-0 dispaly-4 color5 text-center">{new Date(this.state.block.response.timestamp).toUTCString()}</h3>
                                </div>
                              </div>
                          </div>
                          <div id="collapse1" className="collapse" aria-labelledby="1" data-parent="#accordion">
                            <div className="card-body">
                                <div className="row mb-2">
                                    <div className="col-md-6">
                                      <p><strong>Previous Hash : </strong><br/>{(this.state.block.response.blockId - 1) !== -1 ? <a href={`/Block/${this.state.block.response.blockId - 1}`}><code>{this.state.block.response.previousHash.substr(7)}</code></a> : '0'}</p>
                                      <p><strong>Hash : </strong><br/><code>{this.state.block.response.hash.substr(7)}</code></p>
                                    </div>
                                    <div className="col-md-6">
                                      <strong>Awarded To: </strong>
                                        <div className="card align-self-center color18-bg color4">
                                          <div className="card-body text-center px-0 align-self-center">
                                            <pre className="color3 ">{this.state.block.response.awardWallet}</pre>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                                  {this.state.block.response.transactions !== undefined ?                                   
                                  <div id="accordion_2">
                                    {this.state.block.response.transactions.map(transaction => {                                    
                                      return(
                                        <div className="card" key={'trx' + transaction.transactionIndex}>
                                          <div className="card-header color8-bg" id="headingOne">
                                            <h3 className="mb-0 color4" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                              <div className="row">
                                                <div className="col"><strong>Transactions Index  </strong><a href={`/Transactions/Index/${transaction.transactionsIndex}`}>{transaction.transactionsIndex}</a></div>
                                                <div className="col"><strong>Amount  </strong>{transaction.amount} keys</div>
                                              </div>
                                            </h3>
                                          </div>
                                      
                                          <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordion_2">
                                            <div className="card-body">
                                              <div  className="row ">
                                                <div className="col-md-12 col-lg-6">
                                                  <strong>Previous Hash  </strong>{ transaction.data.length ? <Link to={`/Transactions/Index/${transaction.transactionsIndex - 1}`}><code>{transaction.txPreviousHash.substr(7)}</code></Link> : "NONE"}
                                                </div>
                                                <div className="col-md-12 col-lg-6">
                                                  <strong>Hash </strong><code>{transaction.txHash.substr(7)}</code>
                                                </div>
                                              </div>
                                              <div className="row ">
                                                <div className="col-sm-12 col-md-6">
                                                  <strong>Sender : </strong>
                                                  <div className="card align-self-center color18-bg">
                                                    <div className="card-body text-center px-0 align-self-center">
                                                      <pre className="color3 mx-0 my-0">{transaction.sender}</pre>
                                                    </div>
                                                  </div>
                                                </div>
                    
                                                <div className="col-sm-12 col-md-6 ">
                                                  <strong>Receiver : </strong>
                                                  <div className="card align-self-center color18-bg">
                                                    <div className="card-body text-center px-0 align-self-center">
                                                      <pre className="color3 mx-0 my-0">{transaction.receiver}</pre>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div className="row pt-2">
                                                <div className="col">
                                                  <ul className="nav nav-tabs" role="tablist">
                                                  {(transaction.data[0].length !== 0) ?
                                                    <li className="nav-item">
                                                      <a className="nav-link active" href="#message" role="tab" data-toggle="tab">Message</a>
                                                    </li>
                                                    : ''}
                                                    <li className="nav-item">
                                                      <a className="nav-link" href="#buzz" role="tab" data-toggle="tab">Data-Segments</a>
                                                    </li>
                                                  </ul>
                                                  
                                                  <div className="tab-content">
                                                    <div role="tabpanel" className="tab-pane fade active show" id="message">
                                                      {(transaction.data[0].length !== 0) ?
                                                      <div className="card align-self-center color18-bg">
                                                        <div className="card-body px-0 align-self-center">
                                                          <pre className="mx-0 my-0">{transaction.data[0].message}</pre>
                                                        </div>
                                                      </div>
                                                      : ''}
                                                    </div>
                                                    <div role="tabpanel" className="tab-pane fade" id="buzz">
                                                      <div className="card align-self-center color16-bg">
                                                        <div className="card-body px-0 align-self-center">
                                                            Storing public transaction on the blockchain.
                                                        </div>
                                                      </div>
                                                    </div>
                                                    
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        )})}</div>:''}                                
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          : ''}
          <Footer />
        </div>
      );
    }
  }

export default withRouter(connect()(NBlock));