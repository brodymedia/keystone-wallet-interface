import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Header from './../Header';
import axios from 'axios';
import HOST_URL from '../../configs';
class WalletOverview extends Component {
    componentWillMount(){
      axios.get(`${HOST_URL}/Wallet/Balance`, {headers:{Authorization:this.props.auth.wallet.walletPublicKey.split('\n').join('\\n')}})
      .then((response) => {
        this.setState({
         balance:response.data.balance
        });  
      })
      .catch(err => {console.log(err);});
      axios.get(`${HOST_URL}/Wallet/Transaction`, {headers:{Authorization:this.props.auth.wallet.walletPublicKey.split('\n').join('\\n')}})
      .then((response) => {
        console.log(response);
        this.setState({
         transactions:response.data.transactions
        });  
      })
      .catch(err => {console.log(err);});
      axios.get(`${HOST_URL}/Wallet/Awards`, {headers:{Authorization:this.props.auth.wallet.walletPublicKey.split('\n').join('\\n')}})
      .then((response) => {
        this.setState({
         blocks:response.data.blocks
        });  
      })
      .catch(err => {console.log(err);});
    }
    render() {
      const imageQR = this.props.auth.wallet.getQRImage();
      return (
        <div>
          <Header />

          Balance : {this.state ? this.state.balance: 0}
          <pre>{this.props ? this.props.auth.wallet.walletPublicKey : ''}</pre>
          <div className="thumbnail" dangerouslySetInnerHTML={{ __html: imageQR }} />
          {this.state ? console.log(this.state.blocks) : ''}
          {this.state ? console.log(this.state.transactions) : ''}
        </div>

 
      );
    }
  }
  
  const mapStateToProps = state => {
  // add spent tansactions and unspent transactions
  return {
    auth: state.auth,
    error: state.auth.error
  };
};

export default withRouter(connect(mapStateToProps)(WalletOverview));
