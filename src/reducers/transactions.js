import {
    MISSING_PARAMETER,
    INVALID_BALANCE,
    INVALID_KEY,
    TRANSACTION_REJECTED,
    TRANSACTION_RECORDED,
  } from '../actions';
  
  export default (transaction = {}, action) => {
    switch (action.type) {
      case MISSING_PARAMETER:
        return { ...transaction, recorded: false, error: action.payload};
      case INVALID_BALANCE:
        return { ...transaction, recorded: false, error: action.payload };
      case INVALID_KEY:
        return { ...transaction, recorded: false, error: action.payload };
      case TRANSACTION_REJECTED:
        return { ...transaction, recorded: false, error: action.payload };
      case TRANSACTION_RECORDED:
        return { ...transaction, recorded: true, recordedTransaction: action.payload ,  error: null};
      default:
        return transaction;
    }
  };