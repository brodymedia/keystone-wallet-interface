# KEYSTONE CURRENCY WALLET INTERFACE #

Use this application to create your own wallet interface
and connect and send transaction on the keystone blockchain. This repo is maintained by the founders of keystonecurrency.com

### Help & The community ###

We encourge you to create pull request's and help us make the wallet interface into an awesome resource for others to interact with the keystone blockchain.